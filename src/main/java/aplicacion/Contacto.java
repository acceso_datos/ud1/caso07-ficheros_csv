package aplicacion;
import com.opencsv.bean.CsvBindByName;

public class Contacto {
	@CsvBindByName(column="Identificador")
	private int identificador;
	@CsvBindByName(column="Nombre_Companyia")
	private String nomCompanyia;
	@CsvBindByName(column="Nombre_Contacto")
	private String nomContacto;
	@CsvBindByName(column="Direccion")
	private String direccion;
	@CsvBindByName(column="Ciudad")
	private String ciudad;
	@CsvBindByName(column="Pais")
	private String pais;
	
	public Contacto() {
		super();
	}

	public Contacto(int identificador, String nomCompanyia, String nomContacto, String direccion, String ciudad, String pais) {
		super();
		this.identificador = identificador;
		this.nomCompanyia = nomCompanyia;
		this.nomContacto = nomContacto;
		this.direccion = direccion;
		this.ciudad = ciudad;
		this.pais = pais;
	}

	public int getIdentificador() {
		return identificador;
	}

	public void setIdentificador(int identificador) {
		this.identificador = identificador;
	}

	public String getNomCompanyia() {
		return nomCompanyia;
	}

	public void setNomCompanyia(String nomCompanyia) {
		this.nomCompanyia = nomCompanyia;
	}

	public String getNomContacto() {
		return nomContacto;
	}

	public void setNomContacto(String nomContacto) {
		this.nomContacto = nomContacto;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	@Override
	public String toString() {
		return "Contacto [identificador=" + identificador + ", nomCompanyia=" + nomCompanyia + ", nomContacto="
				+ nomContacto + ", direccion=" + direccion + ", ciudad=" + ciudad + ", pais=" + pais + "]";
	}	
	
	
}
